import math


def get_next(x0, x1):
	x2 = (x1+x0)/2
	h0, h1 = x1-x0,	x2-x1
	if h0 == 0: return x1
	d0, d1 = (f(x1) - f(x0))/h0, (f(x2) - f(x1))/h1
	a = (d1-d0)/(h1-h0)
	b, c = a*h1 + d1, f(x2)
	if (b >= 0):
		return x2 + (-2*c)/(b+math.sqrt(b*b - 4*a*c))
	else:
		return x2 + (-2*c)/(b-math.sqrt(b*b - 4*a*c))

# criteria = 0 max_interaction
# criteria = 1 min_error
def check_criteria(old, new, criteria, criteria_value, iterator):
	if criteria == 0:
		return iterator<criteria_value
	elif criteria == 1:
		return ((new-old)/new) * 100 > criteria_value

def muller_method(x0, x1, criteria = 0, criteria_value=10):
	iterator = 0
	check = True
	while(check):
		check = check_criteria(x0, x1, criteria, criteria_value, iterator)
		temp = x1
		x1 = get_next(x0, x1)
		x0 = temp
		iterator += 1
	print("O valor aproximado da raiz é de " + str(x1))


# target function
def f(x):
	return (math.pow(x, 3) - 13*x - 12)

muller_method(0, 10, 0, 10)